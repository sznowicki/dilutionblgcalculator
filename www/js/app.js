// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('DillutionBLGCalculator', ['ionic']);

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'templates/home.html'
      });
  $urlRouterProvider.otherwise('/');
});

app.controller('MainCtrl', function($scope) {


});

app.directive('blg',['$ionicPopup', function($ionicPopup){
    return {
        restrict: 'C',
        link: function(scope, element, atts) {
            var el = element[0],
                go = el.querySelector('#go'),
                inputA = el.querySelector('#inputA'),
                inputB = el.querySelector('#inputB'),
                inputC = el.querySelector('#inputC'),
                resultContainer = el.querySelector('#result');

            angular.element(go).bind('click', function(){
                var A = inputA.value,
                    B = inputB.value,
                    C = inputC.value;

                try {
                    var result = getResult(A, B, C);
                } catch(e) {
                    $ionicPopup.alert({
                        title: 'Błąd',
                        template: e.message
                    });

                    return false;
                }

                var resultString = 'Trzeba dolać <strong>' + result + 'l</strong> wody';

                resultContainer.innerHTML = resultString;
            });

            function getResult(a,b,c) {
                validateInput(a);
                validateInput(b);
                validateInput(c);

                var res = ((a-b)*c)/b;

                return res.toFixed(2);

                function validateInput(i) {
                    if (isNaN(parseFloat(i))) {
                        throw new Error('Proszę podać wszyskie wartości');
                    }
                }
            }
        }
    }

}]);
